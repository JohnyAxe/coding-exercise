package gamesys.registration_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import gamesys.registration_service.config.RegistrationWebConfig;

@SpringBootApplication
@Import({
        RegistrationWebConfig.class
})
public class RegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationApplication.class, args);
	}
}
