package gamesys.registration_service.domain;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import gamesys.registration_service.validators.ValidSSNMember;

/**
 * Member Domain Class
 *
 */
@ValidSSNMember
public class User {

    @NotNull
    private String userName;

    @NotNull
    private String password;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private String birthDate;

    @NotNull
    private String ssn;

    public User() {
        super();
    }

    /**
     * @param userName
     * @param password
     * @param birthDate
     * @param ssw
     */
    public User(String userName, String password, String birthDate, String ssn) {
        super();
        this.userName = userName;
        this.password = password;
        this.birthDate = birthDate;
        this.ssn = ssn;
    }

    /**
     * @return the userName
     */
    public String getUserName() {

        return userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {

        return password;
    }

    /**
     * @return the birthDate
     */
    public String getBirthDate() {

        return birthDate;
    }

    /**
     * @return the ssw
     */
    public String getSsn() {

        return ssn;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {

        return Objects.hash(userName, password, birthDate, ssn);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object thatObj) {

        if (this == thatObj) {
            return true;
        }
        if (!(thatObj instanceof User)) {
            return false;
        }
        User that = (User) thatObj;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(password, that.password) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(ssn, that.ssn);
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        return "Member [userName=" + userName + ", password=" + password + ", birthDate=" + birthDate + ", ssw=" + ssn + "]";
    }

}
