package gamesys.registration_service.services.user_exclusion;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
/**
 * In Memory BlackList service implementation
 *
 */

@Service
public class InMemoryExclusionService implements ExclusionService {

    private static final Logger logger = LoggerFactory.getLogger(InMemoryExclusionService.class);

    private Map<String, String> blackListUsers = new ConcurrentHashMap<>();

    @PostConstruct
    public void initIt() throws Exception {

        blackListUsers.put("12-3123123", "12-05-1855");
        blackListUsers.put("1231231543", "15-09-1986");
        blackListUsers.put("123123-1543", "15-09-1986");

    }


    /*
     * (non-Javadoc)
     *
     * @see
     * gamesys.registration_service.services.ExclusionService#validate(java.lang
     * .String, java.lang.String)
     */
    @Override
    public boolean validate(String dob, String ssn) {

        boolean isBlacklisted = !(blackListUsers.containsKey(ssn) && blackListUsers.get(ssn).equals(dob)) ? true : false;
        logger.debug("The user '{}' is valid '{}'", ssn, isBlacklisted);
        return isBlacklisted;
    }

}
