package gamesys.registration_service.services.user_registry;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gamesys.registration_service.domain.User;
import gamesys.registration_service.services.user_exclusion.ExclusionService;
import gamesys.registration_service.web.exceptions.UserBlackListedException;
import gamesys.registration_service.web.exceptions.UserRegisteredException;

/**
 * In Memory User Registry
 *
 */
@Service
public class InMemoryUserRegistry implements UserRegistry {


    private static final Logger logger = LoggerFactory.getLogger(InMemoryUserRegistry.class);

    @Autowired
    private ExclusionService exclusionService;


    private Map<String, User> registeredUsers = new ConcurrentHashMap<>();

    /*
     * (non-Javadoc)
     *
     * @see gamesys.registration_service.services.user_registry.UserRegistry#
     * registerUser(gamesys.registration_service.domain.User)
     */
    @Override
    public void registerUser(User user) {

        saveUser(user);

    }

    /**
     * @param user
     *            to save
     */
    private void saveUser(User user) {

        if (exclusionService.validate(user.getBirthDate(), user.getSsn())) {
            if (!this.registeredUsers.containsKey(user.getSsn())) {
                logger.debug("Adding New User '{}' to the Register", user.getSsn());
                registeredUsers.put(user.getSsn(), user);
                try {
                    Thread.sleep(2000);
                }
                catch (InterruptedException ex) {
                    logger.debug("Error saving user : '{}'", ex.getMessage());
                }

                return;
            }
            logger.debug("User '{}' registered", user.getSsn());
            throw new UserRegisteredException(user.getSsn());
        }
        logger.debug("User '{}' blacklisted", user.getSsn());
        throw new UserBlackListedException(user.getSsn());
    }




}
