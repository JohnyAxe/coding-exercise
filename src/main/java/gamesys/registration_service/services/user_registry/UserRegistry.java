package gamesys.registration_service.services.user_registry;

import gamesys.registration_service.domain.User;

/**
 * User Registry
 *
 */

public interface UserRegistry {

    void registerUser(User user);


}
