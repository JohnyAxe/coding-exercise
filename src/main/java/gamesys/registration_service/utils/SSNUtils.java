package gamesys.registration_service.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Social Security Number utils
 *
 */
public class SSNUtils {

    /**
     *
     */
    private static final String SSN_REGEX = "^\\d{3}[- ]?\\d{2}[- ]?\\d{4}$";

    /**
     * isSSNValid: Validate Social Security number (SSN) using Java reg ex. This
     * method checks if the input string is a valid SSN.
     *
     * @param email
     *            String. Social Security number to validate
     * @return boolean: true if social security number is valid, false
     *         otherwise.
     */
    public static boolean isSSNValid(String ssn) {

        boolean isValid = false;
        /*
         * SSN format xxx-xx-xxxx, xxxxxxxxx, xxx-xxxxxx; xxxxx-xxxx: ^\\d{3}:
         * Starts with three numeric digits. [- ]?: Followed by an optional "-"
         * \\d{2}: Two numeric digits after the optional "-" [- ]?: May contain
         * an optional second "-" character. \\d{4}: ends with four numeric
         * digits.
         *
         * Examples: 879-89-8989; 869878789 etc.
         */

        // Initialize reg ex for SSN.
        Pattern pattern = Pattern.compile(SSN_REGEX); // myString.matches("\\d{3}-\\d{2}-\\d{4}");
        Matcher matcher = pattern.matcher(ssn);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
