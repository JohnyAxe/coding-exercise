package gamesys.registration_service.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import gamesys.registration_service.domain.User;
import gamesys.registration_service.utils.SSNUtils;

/**
 * SSN Member Validator
 *
 */
public class SSNMemberValidator implements ConstraintValidator<ValidSSNMember, User> {

    private String userMessage;
    /*
     * (non-Javadoc)
     *
     * @see
     * javax.validation.ConstraintValidator#initialize(java.lang.annotation.
     * Annotation)
     */
    @Override
    public void initialize(ValidSSNMember constraintAnnotation) {

        userMessage = constraintAnnotation.message();
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
     * javax.validation.ConstraintValidatorContext)
     */
    @Override
    public boolean isValid(User member, ConstraintValidatorContext context) {

        return SSNUtils.isSSNValid(member.getSsn());
    }

}
