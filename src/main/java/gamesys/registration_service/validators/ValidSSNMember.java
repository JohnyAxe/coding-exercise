package gamesys.registration_service.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * <class description>
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { SSNMemberValidator.class })
public @interface ValidSSNMember {

    String message() default "Please provide an valid SSN"; // "{com.registration.service.validators.ssn.message}"

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
