package gamesys.registration_service.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import gamesys.registration_service.domain.User;
import gamesys.registration_service.services.user_registry.UserRegistry;

/**
 * Regestry Endpoint to register new User
 *
 */
@RequestMapping(Registry.REGISTRY_ENDPOINT)
@RestController
public class Registry {

    public static final String REGISTRY_ENDPOINT = "/registry";

    public static final String USER_ENDPOINT = "/user";

    @Autowired
    private UserRegistry userRegistry;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> registerUser(@Valid
    @RequestBody
    final User userToRegister) {

        userRegistry.registerUser(userToRegister);
        return new ResponseEntity<>(HttpStatus.OK); // only if no exceptions are
                                                    // thrown
    }
}
