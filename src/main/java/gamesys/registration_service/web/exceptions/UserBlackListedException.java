package gamesys.registration_service.web.exceptions;


/**
 * User BlackListedException
 *
 */
public class UserBlackListedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -4239560026376610887L;

    public UserBlackListedException(String userId) {
        super("could not find user '" + userId + "'.");
    }
}