package gamesys.registration_service.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * <class description>
 *
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class UserRegisteredException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public UserRegisteredException(String userId) {
        super("User [" + userId + "] already exists.");
    }

}
