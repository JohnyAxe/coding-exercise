package gamesys.registration_service.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * <class description>
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserSSNNotFoundException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = -4239560026376610887L;

    public UserSSNNotFoundException(String userId) {
        super("could not find user '" + userId + "'.");
    }
}