package gamesys.registration.utils;

import org.junit.Assert;
import org.junit.Test;

import gamesys.registration_service.utils.SSNUtils;
/**
 * SSN Utils Test Suite
 *
 */
public class SSNUtilsTest {


    @Test
    public void isValidSSNWithAllOptionalHyfenChars() {

        // Given
        String ssn = "879-89-8989";

        // When Then
        Assert.assertTrue("Should use an valid SSN", SSNUtils.isSSNValid(ssn));

    }

    @Test
    public void isValidSSNWithoutAllOptionalHyfenChars() {

        // Given
        String ssn = "879898989";

        // When Then
        Assert.assertTrue("Should use an valid SSN", SSNUtils.isSSNValid(ssn));

    }

    @Test
    public void isValidSSNWithFirstOptionalHyfenChar() {

        // Given
        String ssn = "879-898989";

        // When Then
        Assert.assertTrue("Should use an valid SSN", SSNUtils.isSSNValid(ssn));

    }

    @Test
    public void isValidSSNWithFLastOptionalHyfenChar() {

        // Given
        String ssn = "87989-8989";

        // When Then
        Assert.assertTrue("Should use an valid SSN", SSNUtils.isSSNValid(ssn));

    }

}
