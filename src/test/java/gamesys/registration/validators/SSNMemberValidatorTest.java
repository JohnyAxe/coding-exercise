package gamesys.registration.validators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import gamesys.registration_service.domain.User;

/**
 * SSN Member Validator Test
 *
 */
public class SSNMemberValidatorTest {


    @Autowired
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testCorrectSSN() {
        //Given
        String name = "Joao";
        String passw = "passw";
        String bDate = "12-05-1855";
        String ssn = "879-898989";

        User member = new User(name, passw, bDate, ssn);


        //
        Set<ConstraintViolation<User>> violations = validator.validate(member);

        //Then
        assertTrue(violations.isEmpty());
    }

    @Test
    public void testIncorrectSSN() {

        // Given
        String name = "Joao";
        String passw = "passw";
        String bDate = "12-05-1855";
        String ssn = "879--898989";

        User member = new User(name, passw, bDate, ssn);

        //
        Set<ConstraintViolation<User>> violations = validator.validate(member);

        // Then
        assertFalse(violations.toString(), violations.isEmpty());
    }


}
