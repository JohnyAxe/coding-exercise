package gamesys.registration.web.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.RestTemplate;

import gamesys.registration_service.config.RegistrationWebConfig;
import gamesys.registration_service.domain.User;

/**
 * Registration Service LiveTest
 *
 */
@SpringBootTest
@ContextConfiguration(classes = { RegistrationWebConfig.class }, loader = AnnotationConfigContextLoader.class)
public class RegistrationServiceLiveTest {

    /**
     * Registry URL
     */
    private static final String REGISTRY_URL_FORMAT = "http://localhost:8080/registry/";

    /**
     * Registry URL
     */
    private static final String REGISTRY_USER_URL_FORMAT = REGISTRY_URL_FORMAT + "user";

    private RestTemplate template;


    private List<String> registered;

    @Before
    public void setUp() throws Exception {

        template = new RestTemplate();

        registered = new ArrayList<>();

        String ssn = "879-8989";

        Random randomGenerator = new Random();
        // Given 10 random SSN
        for (int idx = 0; idx < 50; ++idx) {
            int randomInt = randomGenerator.nextInt(90) + 10;
            String randomSSN = ssn + randomInt;

            if (!registered.contains(randomSSN)) {
                registered.add(randomSSN);
            }

        }
    }

    @Test
    public void newUserRestTest() throws InterruptedException, ExecutionException {

        // Given
        String name = "Joao";
        String passw = "passw";
        String bDate = "12-05-1855";
        String ssn = "879-898989";

        User user = new User(name, passw, bDate, ssn);

        // Given
        ResponseEntity<String> userCreated = template.postForEntity(REGISTRY_USER_URL_FORMAT, user, String.class);

        // Then
        assertThat(userCreated, notNullValue());
        assertThat(userCreated.getStatusCode(), IsEqual.equalTo(HttpStatus.OK));

    }

    @Test
    public void performaceSyncUserRestTest() throws InterruptedException, ExecutionException {

        // Given
        String name = "Joao";
        String passw = "passw";
        String bDate = "12-05-1855";

        // When

        List<CompletableFuture<?>> crazyAirResponse = new ArrayList<CompletableFuture<?>>();

        this.registered.stream().forEach(ssn -> {

            CompletableFuture<Void> completableFuture = new CompletableFuture<>();

            Executors.newCachedThreadPool().submit(() -> {
                User user = new User(name, passw, bDate, ssn);
                ResponseEntity<String> userCreated = template.postForEntity(REGISTRY_USER_URL_FORMAT, user, String.class);

                // Then
                assertThat(userCreated, notNullValue());
                assertThat(userCreated.getStatusCode(),
                        IsEqual.equalTo(HttpStatus.OK));

                completableFuture.complete(null);
                return null;
            });

            crazyAirResponse.add(completableFuture);

        }); // 12

        CompletableFuture.allOf(crazyAirResponse.toArray(new CompletableFuture<?>[crazyAirResponse.size()])).join();

    }


}
